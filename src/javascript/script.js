//= ../../node_modules/jquery/dist/jquery.min.js
//= ../../node_modules/jquery-migrate/dist/jquery-migrate.min.js
//= ../../node_modules/instafeed.js/instafeed.min.js
//= ../../node_modules/svg4everybody/dist/svg4everybody.min.js
//= ../../node_modules/rellax/rellax.min.js

document.addEventListener("DOMContentLoaded", () => {
  "use strict";

  // Scrolling to anchor links
  const root = (() => {
    if ("scrollingElement" in document) return document.scrollingElement;
    const html = document.documentElement;
    const start = html.scrollTop;
    html.scrollTop = start + 1;
    const end = html.scrollTop;
    html.scrollTop = start;
    return end > start ? html : document.body;
  })();

  const ease = (duration, elapsed, start, end) =>
  Math.round(end * (-Math.pow(2, -10 * elapsed/duration) + 1) + start);

  const getCoordinates = hash => {
    const start = root.scrollTop;
    const delta = (() => {
      if (hash.length < 2) return -start;
      const target = document.querySelector(hash);
      if (!target) return;
      const top = target.getBoundingClientRect().top;
      const max = root.scrollHeight - window.innerHeight;
      return start + top < max ? top : max - start;
    })();
    if (delta) return new Map([["start", start], ["delta", delta]]);
  };

  const scroll = link => {
    const hash = link.getAttribute("href");
    const coordinates = getCoordinates(hash);
    if (!coordinates) return;

    const tick = timestamp => {
      progress.set("elapsed", timestamp - start);
      root.scrollTop = ease(...progress.values(), ...coordinates.values());
      progress.get("elapsed") < progress.get("duration")
      ? requestAnimationFrame(tick)
      : complete(hash, coordinates);
    };

    const progress = new Map([["duration", 1300]]);
    const start = performance.now();
    requestAnimationFrame(tick);
  };

  const complete = (hash, coordinates) => {
    history.pushState(null, null, hash);
    root.scrollTop = coordinates.get("start") + coordinates.get("delta");
  };

  const attachHandler = (links, index) => {
    const link = links.item(index);
    link.addEventListener("click", event => {
      event.preventDefault();
      scroll(link);
    });
    if (index) return attachHandler(links, index - 1);
  };

  const links = document.querySelectorAll("a.scroll");
  const last = links.length - 1;
  if (last < 0) return;
  attachHandler(links, last);


  // Add SVG External content support to all browsers
  svg4everybody()


  // Add Instagram gallery
  var feed = new Instafeed({
    get: 'tagged',
    tagName: 'awesome',
    clientId: 'YOUR_CLIENT_ID'
  });
  feed.run();

  // Parallax
  const rellax = new Rellax('.rellax');

    // Responsive menu
  const burger = document.querySelector('.burger');
  const menu = document.querySelector('.mobile-nav');
  const burgerClose = document.querySelector('.mobile-nav__close-icon');
  const win = window;

  function openMenu(event) {
    menu.classList.toggle('mobile-nav--is-open');
    event.preventDefault();
    event.stopImmediatePropagation();
  }

  function closeMenu() {
    if ( menu.classList.contains('mobile-nav--is-open') ) {
      menu.classList.remove('mobile-nav--is-open');
    }
  }

  burger.addEventListener('click', openMenu, false);
  burgerClose.addEventListener('click', closeMenu, false);
  win.addEventListener('click', closeMenu, false);

  });

$(document).ready(function() {

  function submitForm(){

    var name = $("#name").val();
    var phone = $("#phone").val();

    $.ajax({
     type: "POST",
     url: "../contact.php",
     data: "&name="+ name + "&phone="+ phone,
     success : function(text){
       if(text == "success"){
         formSuccess();
       }
     }
   });
  }

  $("#order-tour-submit").submit(function(event){

   event.preventDefault();
   submitForm();

  });

});